import 'package:flutter/material.dart';

void main() => runApp(JogoForca());

class JogoForca extends StatefulWidget {
  @override
  State<JogoForca> createState() => _JogoForcaState();
}

class _JogoForcaState extends State<JogoForca> {
  @override
  void initState() {
    super.initState();
    // este é o primeiro método a ser executado quando seu aplicativo abrir.
    // use-o para configurar as variáveis iniciais, por exemplo.
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jogo Forca',
      home: Scaffold(),
    );
  }
}
